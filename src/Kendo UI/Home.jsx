import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import Signin from './Signin';
import Signup from './Signup';

export default function Home() {
  return (
    <Router>
      <Switch>
        <Route exact path='/' component={Signin}/>
        <Route exact path='/signup' component={Signup}/>
      </Switch>
    </Router>
  );
}