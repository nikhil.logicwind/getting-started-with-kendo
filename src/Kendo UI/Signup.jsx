import React from 'react'
import { Form, Field, FormElement } from "@progress/kendo-react-form";
import { Button } from "@progress/kendo-react-buttons";
import {
  FormDatePicker,
  FormInput,
  FormCheckbox,
  FormMaskedTextBox,
  FormTextArea,
  FormRadioGroup
} from "./components/form-components";
import {
  termsValidator,
  emailValidator,
  nameValidator,
  phoneValidator,
  arrivalDateValidator,
  userNameValidator
} from "./components/Validators";
const genders = [
  {
    label: "Male",
    value: "male",
  },
  {
    label: "Female",
    value: "female",
  },
  {
    label: "Other",
    value: "other",
  },
];

export default function Forms() {
  const handleSubmit = (dataItem) => alert(JSON.stringify(dataItem, null, 2));
  return (
    <Form
      style={{height: "auto"}}
      onSubmit={handleSubmit}
      render={(formRenderProps) => (
        <FormElement
          style={{
            width: 400,
          }}
        >
          <fieldset className={"k-form-fieldset"}>
            <Field
              id={"fullName"}
              name={"fullName"}
              label={"Full Name"}
              component={FormInput}
              validator={nameValidator}
            />
            <Field
            id={"username"}
            name={"username"}
            label={"User Name"}
            component={FormInput}
            validator={userNameValidator}
          />
            <div
              style={{
                display: "flex",
                justifyContent: "space-between",
              }}
            >
              <Field
                id={"BirthDate"}
                name={"BirthDate"}
                label={"Birth Date"}
                hint={"Note: Shouldn't be greater than today"}
                component={FormDatePicker}
                validator={arrivalDateValidator}
                wrapperStyle={{
                  width: "90%",
                  marginRight: "18px",
                }}
              />
            </div>
            <Field
              id={"gender"}
              name={"gender"}
              label={"Gender"}
              layout={"horizontal"}
              component={FormRadioGroup}
              data={genders}
            />
            <Field
              id={"phoneNumber"}
              name={"phoneNumber"}
              label={"Phone Number"}
              mask={"(+00) 0000000000"}
              hint={"Note: Your active phone number."}
              component={FormMaskedTextBox}
              validator={phoneValidator}
            />
            <Field
              id={"email"}
              name={"email"}
              label={"Email"}
              hint={"Note: Enter your personal email address."}
              type={"email"}
              component={FormInput}
              validator={emailValidator}
            />
            <span className={"k-form-separator"} />
            <Field
              id={"terms"}
              name={"terms"}
              label={"I agree to provide my details to logicwind"}
              component={FormCheckbox}
              validator={termsValidator}
            />
            <div className="k-form-buttons">
              <Button
                primary={true}
                type={"submit"}
                disabled={!formRenderProps.allowSubmit}
              >
                Register
              </Button>
              <Button onClick={formRenderProps.onFormReset}>Clear</Button>
            </div>
          </fieldset>
        </FormElement>
      )}
    />
  )
}
